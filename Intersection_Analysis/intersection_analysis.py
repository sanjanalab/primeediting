# Written by Jahan Rahman on 2020/01/04
#Used to discover intersections with common human SNPs and the designed prime editing reagents
import glob, os

def pull():
	files = glob.glob("/gpfs/commons/home/jrahman/sanjana-lab/intersection_analysis/sorted_lists/*")
	return files

def get_entries(dataframe):
	open_file = open(dataframe, "r")
	entries = open_file.read().splitlines()
	entries.remove(entries[0])
	return entries

def get_prefix(filepath):
	prefix = filepath.split("/")[-1].split(".")[0].strip()
	return prefix

def formatter_PE2(data, file_prefix):
	new_file_name_before = "/gpfs/commons/home/jrahman/sanjana-lab/intersection_analysis/unsorted_bedfiles/" + file_prefix + "_before.bed"
	open_file_before = open(new_file_name_before, "w+")
	new_file_name_after = "/gpfs/commons/home/jrahman/sanjana-lab/intersection_analysis/unsorted_bedfiles/" + file_prefix + "_after.bed"
	open_file_after = open(new_file_name_after, "w+")
	new_file_name_pbs = "/gpfs/commons/home/jrahman/sanjana-lab/intersection_analysis/unsorted_bedfiles/" + file_prefix + "_pbs.bed"
	open_file_pbs = open(new_file_name_pbs, "w+")
	output_entries_before = []
	output_entries_after =[]
	output_entries_pbs = []
	for i in data:
		columns = i.split("\t")
		identifier = (columns[0].strip() + "-" + columns[1].strip() + "-" + columns[6].strip() + "-" + columns[7].strip()).replace(" ", "").replace("_", "-").replace(":", "-").strip() + "-" + columns[2].strip().replace("+", "plus").replace("-", "minus")
		before_target_chromosome = columns[17].strip()
		before_target_start = str(columns[18])
		before_target_end = columns[19].strip()
		after_target_chromosome = columns[20].strip()
		after_target_start = str(columns[21])
		after_target_end = columns[22].strip()
		pbs_target_chromosome = columns[23].strip()
		pbs_target_start = columns[24].strip()
		pbs_target_end = columns[25].strip()
		output_entry_before = '\t'.join([before_target_chromosome, before_target_start, before_target_end, identifier])
		output_entry_after = '\t'.join([after_target_chromosome, after_target_start, after_target_end, identifier])
		pbs_entry = '\t'.join([pbs_target_chromosome, pbs_target_start, pbs_target_end, identifier])
		if before_target_start != "NA" and before_target_end != "NA":
			output_entries_before.append(output_entry_before)
		if after_target_start != "NA" and after_target_end != "NA":
			output_entries_after.append(output_entry_after)
		if pbs_target_start != "NA" and pbs_target_end != "NA":
			output_entries_pbs.append(pbs_entry)
	open_file_before.write('\n'.join(output_entries_before))
	open_file_after.write('\n'.join(output_entries_after))
	open_file_pbs.write("\n".join(output_entries_pbs))
	open_file_before.close()
	open_file_after.close()
	open_file_pbs.close()

def formatter_PE3(data, file_prefix):
	new_file_name = "/gpfs/commons/home/jrahman/sanjana-lab/intersection_analysis/unsorted_bedfiles/" + file_prefix + ".bed"
	open_file = open(new_file_name, "w+")
	output_entries = []
	for i in data:
		columns = i.split("\t")
		identifier = columns[6].replace("+", "plus").replace("-r-b", "rb").replace("-b", "b").replace("-", "minus").replace("_", "-").strip()
		chromosome = columns[8].strip()
		start = str(columns[9])
		end = columns[10].strip()
		output_entry = '\t'.join([chromosome, start, end, identifier])
		output_entries.append(output_entry)
	open_file.write('\n'.join(output_entries))
	open_file.close()

def formatter_PE3b(data, file_prefix):
	new_file_name_before = "/gpfs/commons/home/jrahman/sanjana-lab/intersection_analysis/unsorted_bedfiles/" + file_prefix + "_before.bed"
	open_file_before = open(new_file_name_before, "w+")
	new_file_name_after = "/gpfs/commons/home/jrahman/sanjana-lab/intersection_analysis/unsorted_bedfiles/" + file_prefix + "_after.bed"
	open_file_after = open(new_file_name_after, "w+")
	output_entries_before = []
	output_entries_after = []
	for i in data:
		columns = i.split("\t")
		identifier = columns[9].replace("+", "plus").replace("-r-b", "rb").replace("-b", "b").replace("-", "minus").replace("_", "-").strip()
		before_target_chromosome = columns[10].strip()
		before_target_start = str(columns[11])
		before_target_end = columns[12].strip()
		after_target_chromosome = columns[13].strip()
		after_target_start = str(columns[14])
		after_target_end = columns[15].strip()
		output_entry_before = '\t'.join([before_target_chromosome, before_target_start, before_target_end, identifier])
		output_entry_after = '\t'.join([after_target_chromosome, after_target_start, after_target_end, identifier])
		if before_target_start != "NA" and before_target_end != "NA":
			output_entries_before.append(output_entry_before)
		if after_target_start != "NA" and after_target_end != "NA":
			output_entries_after.append(output_entry_after)
	open_file_before.write('\n'.join(output_entries_before))
	open_file_after.write('\n'.join(output_entries_after))
	open_file_before.close()
	open_file_after.close()

def sort():
	unsorted_files = glob.glob("/gpfs/commons/groups/sanjana_lab/jrahman/intersection_analysis/unsorted_bedfiles/*")
	for i in unsorted_files:
		command = "sort -k 1,1 -k2,2n %s > /gpfs/commons/groups/sanjana_lab/jrahman/intersection_analysis/sorted_bedfiles/%s.bed" % (i, get_prefix(i))
		os.system(command)
		print("Done with sorting {}".format(i))	

def intersect():
	sorted_files = glob.glob("/gpfs/commons/groups/sanjana_lab/jrahman/intersection_analysis/sorted_bedfiles/*")
	for i in sorted_files:
		command = "bedtools intersect -wao -a %s -b dbsnp_sorted.bed > /gpfs/commons/home/jrahman/sanjana-lab/intersection_analysis/intersection_output/%s_intersections.txt" % (i, get_prefix(i))
		os.system(command)
		print("Done with intersection {}".format(i))

def main():
	os.system("mkdir -p unsorted_bedfiles/ sorted_bedfiles/ intersection_output/")
	input_dataframes = pull()
	for i in input_dataframes:
		if "PE2" in i:
			formatter_PE2(get_entries(i), get_prefix(i))
		elif "PE3b" in i:
			formatter_PE3b(get_entries(i), get_prefix(i))
		else: 
			formatter_PE3(get_entries(i), get_prefix(i))
		print("Done with formatting {}".format(i))
	sort()
	intersect()


main()


