# Written by Jahan Rahman on 2020/07/01
#Jahan Rahman Used to run discovery step to generate .output files from fasta file input 
import os, glob, sys

def discover(fastafile, outfilename):
	command = "java -Xmx4g -jar /gpfs/commons/groups/sanjana_lab/jmorris/LabTools/FlashFry.1.10/FlashFry-assembly-1.10.jar discover --database /gpfs/commons/home/jrahman/sanjana-lab/cas9ngg_database  --fasta %s --output %s" % (fastafile.strip(), outfilename) 
	os.system(command)

outfilename = sys.argv[2].strip() + sys.argv[1].split("/")[-1].split(".")[0].strip() + ".output" 

discover(sys.argv[1], outfilename)
print("Discovery Complete")


