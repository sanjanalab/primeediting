# Written by Jahan Rahman on 2020/15/03
#Concatenate desired .scored into .txt files 

import os, glob

files = glob.glob("/gpfs/commons/home/jrahman/sanjana-lab/flashfry/input_data_FF/*")
os.system("mkdir -p concatenated_score_files")

commands = []
for i in files:
	file_prefix = i.split("/")[-1].split(".")[0].strip()
	command = "cat <(head -n 1 -q ./score_files/%s_scored/*.scored | uniq | sed -e 's/contig/ID\\tguide/') <(tail -n +2 -q ./score_files/%s_scored/*.scored | sed -e 's/_/\\t/1' | awk '{if ($2 == $5) print $0}') > ./concatenated_score_files/%s_combined_scores.txt" % (file_prefix, file_prefix, file_prefix)
	commands.append(command)

bashscript = open("/gpfs/commons/home/jrahman/sanjana-lab/flashfry/combinescoresff.sh", "w+")
bashscript.write("#!/bin/bash" + "\n" + "cd /gpfs/commons/home/jrahman/sanjana-lab/flashfry/" + "\n")

bashscript.write("\n".join(commands))

bashscript.close()

os.system("bash /gpfs/commons/home/jrahman/sanjana-lab/flashfry/combinescoresff.sh")
