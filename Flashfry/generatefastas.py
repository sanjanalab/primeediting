# Written by Jahan Rahman on 2020/03/01
#Generate fastas from formatted flashfry bins
import os, glob, sys

def generate_fasta(infile, outdir):
	prefix = infile.split("/")[-1].split(".")[0].strip()
	open_file = open(infile, "r")
	infile_entries = open_file.read().splitlines()
	new_fasta_name = outdir + prefix + ".fasta"
	open_new_fasta = open(new_fasta_name, "w+")
	for i in infile_entries:
		columns = i.split("\t")
		formatted_id = columns[0]
		open_new_fasta.write(">" + formatted_id + "_" + columns[1].strip() + "\n")
		open_new_fasta.write(columns[2].strip() + "\n")
	open_new_fasta.close()

command = "mkdir -p %s" % (sys.argv[2].strip())
os.system(command)

file_list_name = sys.argv[1].strip() + "*"
file_list = glob.glob(file_list_name)

for i in file_list:
	generate_fasta(i, sys.argv[2])


