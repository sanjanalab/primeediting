# Written by Jahan Rahman on 2020/15/03
# Generate preprocessing scripts and flashfry analysis scripts for each input dataframe stored in input_data_FF folder, and executes all steps.
import glob, os

def make_dirs():
	os.system("mkdir -p preprocessing_scripts/ flashfry_scripts/ formatted_files/ broken_up_files/ fasta_files/ output_files/ score_files/ flashfry_logs/")

def generate_preprocessing_scripts(files):
	preprocessing_script_paths = []
	for i in files:
		file_prefix = i.split("/")[-1].split(".")[0].strip()
		if "PE2" in file_prefix:
			parse_script = "parseformatflashfrype2.py"
		elif "PE3" in file_prefix:
			parse_script = "parseformatflashfrype3.py"	
		preprocessing_file_name ="/gpfs/commons/home/jrahman/sanjana-lab/flashfry/preprocessing_scripts/preprocessing_" + file_prefix + ".sh"
		preprocessing_script_paths.append(preprocessing_file_name)
		open_preprocessing_file = open(preprocessing_file_name, "w+")
		open_preprocessing_file.write("#!/bin/bash\ncd /gpfs/commons/groups/sanjana_lab/jrahman/flashfry/\n")
		command = "python %s ./input_data_FF/%s.toscore.txt  ./formatted_files/%s_flashfry_formatted.txt && python breakupinput.py ./formatted_files/%s_flashfry_formatted.txt ./broken_up_files/%s/ && python generatefastas.py ./broken_up_files/%s/ ./fasta_files/%s_fasta/\n" % (parse_script, file_prefix, file_prefix, file_prefix, file_prefix, file_prefix, file_prefix)
		open_preprocessing_file.write(command)
		open_preprocessing_file.close()
	return preprocessing_script_paths

def generate_flashfry_scripts(files):
	flashfry_script_paths = []
	for i in files:
		file_prefix = i.split("/")[-1].split(".")[0].strip()
		flashfry_file_name = "/gpfs/commons/home/jrahman/sanjana-lab/flashfry/flashfry_scripts/flashfryanalysis_" + file_prefix + ".sh"
		flashfry_script_paths.append(flashfry_file_name)
		open_flashfry_file = open(flashfry_file_name, "w+")
		command = "#!/bin/bash\n#\n#SBATCH --mail-user=email@nygenome.org\n#SBATCH --mail-type=END\n#SBATCH --job-name=flashfry_%s\n#SBATCH --output=/gpfs/commons/home/jrahman/sanjana-lab/flashfry/flashfry_logs/%s-%%j.out\n#SBATCH --error=/gpfs/commons/home/jrahman/sanjana-lab/flashfry/flashfry_logs/%s-%%j.err\n#SBATCH --time=40:00:00\n#SBATCH --nodes=1\n#SBATCH --tasks-per-node=1\n#SBATCH --cpus-per-task=12\n#SBATCH --mem=40GB\n#SBATCH --array=1-12\n\n###-----Statements to be Executed-----###\n\ncd /gpfs/commons/groups/sanjana_lab/jrahman/flashfry\nmkdir -p ./output_files/%s_output ./score_files/%s_scored\nmodule purge\nmodule load java/1.8\npython discover.py ./fasta_files/%s_fasta/%s_flashfry_formatted_part$SLURM_ARRAY_TASK_ID.fasta  ./output_files/%s_output/ && python score.py ./output_files/%s_output/%s_flashfry_formatted_part$SLURM_ARRAY_TASK_ID.output  ./score_files/%s_scored/\n" % (file_prefix, file_prefix, file_prefix, file_prefix, file_prefix, file_prefix, file_prefix, file_prefix, file_prefix, file_prefix, file_prefix)
		open_flashfry_file.write(command)
		open_flashfry_file.close()
	return generate_flashfry_scripts

def generate_preprocessing_execution(paths):
	open_preprocessing_execute_file = open("/gpfs/commons/home/jrahman/sanjana-lab/flashfry/execute_all_preprocessing.sh", "w+")
	open_preprocessing_execute_file.write("#!/bin/bash\n")
	for i in paths:
		command = "bash %s\n" % (i)
		open_preprocessing_execute_file.write(command)
	open_preprocessing_execute_file.close()
	

def generate_flashfry_execution(paths):
	open_flashfry_execute_file = open("/gpfs/commons/home/jrahman/sanjana-lab/flashfry/execute_all_flashfry_analysis.sh", "w+")
	open_flashfry_execute_file.write("#!/bin/bash\n")
	for i in paths:
		command = "sbatch %s\n" % (i)
		open_flashfry_execute_file.write(command)
	open_flashfry_execute_file.close()

def execute_scripts():
	os.system("bash /gpfs/commons/home/jrahman/sanjana-lab/flashfry/execute_all_preprocessing.sh")
	os.system("bash /gpfs/commons/home/jrahman/sanjana-lab/flashfry/execute_all_flashfry_analysis.sh")

def main():
	make_dirs()
	files = glob.glob("/gpfs/commons/home/jrahman/sanjana-lab/flashfry/input_data_FF/*")
	preprocessing_paths = generate_preprocessing_scripts(files)
	flashfry_paths = generate_flashfry_scripts(files)
	generate_preprocessing_execution(preprocessing_script_paths)
	generate_flashfry_execution(flashfry_script_paths)
	execute_scripts()

if __name__ == '__main__':
	main()
