# Written by Jahan Rahman on 2020/03/01
# Create formatted input table for generating fasta files using input PE2 Dataframe
import sys

def get_entries(infile):
	input_open = open(infile, "r")
	input_entries = input_open.read().splitlines() 
	input_entries.remove(input_entries[0])
	return input_entries

def generated_formatted_file(inputs, outfile):
	output_open = open(outfile, "w+")
	entries = []
	for i in inputs:
		columns = i.split("\t")
		identifier = (columns[0].strip() + "-" + columns[1].strip() + "-" + columns[6].strip() + "-" + columns[7].strip()).replace(" ", "").replace("_", "-").replace(":", "-").strip() + "-" + columns[2].strip().replace("+", "plus").replace("-", "minus")
		guide_sequence_with_pam = columns[3].strip() + columns[4].strip()
		flash_fry_seq = "TTCGTACAAA" + columns[3].strip() + columns[4].strip() + "TGTCCGCACT"
		entry = '\t'.join([identifier, guide_sequence_with_pam, flash_fry_seq])
		entries.append(entry)
	output_open.write('\n'.join(entries))
	output_open.close()

def main():
	generated_formatted_file(get_entries(sys.argv[1]), sys.argv[2])

if __name__ == '__main__':
	main()

