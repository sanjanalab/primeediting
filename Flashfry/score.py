# Written by Jahan Rahman on 2020/17/04
#Used to run scoring step to generate .scored files from .output file input 
import os, glob, sys
def score(infile, outfile):
	command = "java -Xmx4g -jar /gpfs/commons/groups/sanjana_lab/jmorris/LabTools/FlashFry.1.10/FlashFry-assembly-1.10.jar score --input %s --output %s --scoringMetrics doench2014ontarget,doench2016cfd,dangerous,hsu2013,minot --database /gpfs/commons/home/jrahman/sanjana-lab/cas9ngg_database" % (infile.strip(), outfile) 
	os.system(command)

outfilename = sys.argv[2].strip() + sys.argv[1].split("/")[-1].split(".")[0].strip() + ".scored" 
score(sys.argv[1], outfilename)
print("Scoring Complete")