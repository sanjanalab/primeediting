# Prime Editing Design: Software for automated design of prime editing reagents

This repository contains software for the design and analysis of prime editing reagents for pathogenic variants in the [ClinVar database](https://www.ncbi.nlm.nih.gov/clinvar/). This repository is intended to accompany our bioRxiv preprint. For more information please refer to:

**Automated design of CRISPR prime editors for thousands of human pathogenic variants**   
John A. Morris*, Jahan A. Rahman*, Xinyi Guo, Neville E. Sanjana. *bioRxiv preprint* (2020)


Instructions:

Here we present a set of R and Python scripts for the design of prime editing guides (both pegRNAs and sgRNAs) and also for interfacing with [FlashFry](https://github.com/mckennalab/FlashFry) for guide scoring.

It is recommended that users read through the code within this repository closely, and make environment-specific modifications in order to acheive the desired design output. The commented sections in the provided scripts provide many details that may help those who wish to better understand the design process for use-case adaptation and troubleshooting.  

For those who would prefer web-based access to pre-designed prime editors for all ClinVar variants, they can be accessed at [primeedit.nygenome.org](http://primeedit.nygenome.org).

If you find our software or pre-designed prime editing reagents useful in your work, we would be very grateful if you could cite the preprint above.

Additional notes:

To run our design tool (see [Design_PE.md](https://gitlab.com/sanjanalab/primeediting/-/blob/master/Design_PE.md) for detailed instruction), several other packages are also needed. Here is the list of these packages and the versions that we used: 
R 3.4.4,
dplyr v0.8.5,
ggplot2 v3.3.0,
data.table v1.12.8,
BSgenome.Hsapiens.UCSC.hg19 v1.4.0,
Biostrings v2.46.0,
future.apply v1.4.0,
FlashFry v1.10, and
bedtools v2.29.2.

If you encounter any errors, please feel free to raise a GitLab issue. If you need any help running these scripts or would like to provide feedback, please reach out to the development team by emailing us at pe@sanjanalab.org.

To properly execute all analysis scripts (those contained within the FlashFry and SNP Intersection directories), modifications will have to be made according to the user's environment. For example, automatedflashfryanalysis.py must be modified to have access to the other dependencies stored in the FlashFry directory of this GitLab page.  

To run FlashFry:
1) Place the desired input reagent data frames (generated from running the design scripts contained within the Design Scripts directory on this GitLab Page) in the input_data_FF/ directory.  Properly formatted input files should have the strings PE2, PE3, or PE3b within their names to ensure correct parsing and formatting of data frames.  Additionally, input files shouldn't contain any "." characters except within the file extension (e.g. ".txt"), as the program splits on this character for its naming convention. 

2) Once all the dependencies are properly located in directories that automatedflashfryscript.py can access, and all fixes to the directories being pointed at are made, the user may then run automatedflashfryscript.py to get off-target discovery and scoring results. Note: This script is designed specifically for use within a slurm workload manager system, so changes will have to be made if other systems are available to the user (e.g SGE). 

3) After the scores are finished computing, run combineflashfryscores.py (again, correcting to appropriate directories) to combine scoring output into files consistent with the original input data.  

4) Final output will be located in the concatenated_score_files/ directory. 


To run the SNP intersection analysis:

1) Download and install [bedtools v2.29.2](https://github.com/arq5x/bedtools2).

2) Download and unzip latest version of common SNPs stored in dbSNP using:
```
 wget http://hgdownload.cse.ucsc.edu/goldenpath/hg19/database/snp151Common.txt.gz
 gunzip snp151Common.txt.gz
```
3) Execute bedformatdbsnp.py to create a common SNPs bed file. Sort the bed file using the following command:
```
sort -k 1,1 -k2,2n dbsnp.bed > dbsnp_sorted.bed
```
4) Ensure the desired input is contained within the sorted_lists/ directory.  The input (which comes from successfully executing the design scripts) must abide by the same naming conventions as described in the flashfry analysis steps (PE2, PE3, or PE3b in name, and no "." except in file extension). Run intersection_analysis.py once all corrections are made to ensure it points to the appropriate directories.

5) Final output will be located within the intersection_output/ directory.