# Tutorial: Designing custom prime editing reagents (R scripts)
John Morris, 5/27/2020

*Updated on 7/15/2021 to include SpRY (near PAM-less) design*

*Here we will go through a multi-species example for prime editor design in six simple steps:*

1. Make the R object for the PE2 design program
2. Run the PE2 design program
3. Make the R object for the PE3 design program
4. Run the PE3 design program to get secondary sgRNAs
5. Make the R object for the PE3b design program
6. Run the PE3b design program to get secondary sgRNAs

Load the required libraries
```{r eval= FALSE}
library(dplyr)
library(data.table)
library(future.apply)
```
Load human, mouse and zebrafish genomes
```{r eval= FALSE}
library(BSgenome.Hsapiens.UCSC.hg19)
library(BSgenome.Mmusculus.UCSC.mm10)
library(BSgenome.Drerio.UCSC.danRer10)
```
Load the DesignPE2, PE3 and PE3b programs
```{r eval= FALSE}
source("bin/DesignPE2.R")
source("bin/DesignPE3.R")
source("bin/DesignPE3b.R")

```
&nbsp;
**Step 1: Make the R object for the PE2 design program**  
  
Zebrafish example: C allele at chr23:27,728,315-27,728,315
```{r eval= FALSE}
ZF <- data.frame('uniq_id' = "eg1.Zebrafish",
                 'chr' = "chr23",
                 'start' = 27728315,
                 'end' = 27728315,
                 'ref' = "C",
                 'alt' = "T",
                 'pbs_size' = 13,
                 'rtt_size' = 16,
                 'pam' = "NGG", # Change to NRN, NYN or NNN if interested in near PAM-less design
                 'genome_build' = "danDer10",
                 stringsAsFactors = F) %>%
  dplyr::mutate(base_rtt_size = 16 - (nchar(ref) - nchar(alt)),
                target_pos_start = start - (start - (17 + base_rtt_size)),
                target_pos_end = end - (start - (17 + base_rtt_size)),
                custom_seq = as.character(getSeq(x = Drerio,
                                                 names = chr,
                                                 start = start - (16 + base_rtt_size),
                                                 end = end + (16 + base_rtt_size))))

```
Mouse example: C allele at chr7:98,847,298-98,847,298
```{r eval= FALSE}
MM <- data.frame('uniq_id' = "eg2.Mouse",
                 'chr' = "chr7",
                 'start' = 98847298,
                 'end' = 98847298,
                 'ref' = "C",
                 'alt' = "A",
                 'pbs_size' = 13,
                 'rtt_size' = 16,
                 'pam' = "NGG", # Change to NRN, NYN or NNN if interested in near PAM-less design
                 'genome_build' = "mm10",
                 stringsAsFactors = F) %>%
  dplyr::mutate(base_rtt_size = 16 - (nchar(ref) - nchar(alt)),
                target_pos_start = start - (start - (17 + base_rtt_size)),
                target_pos_end = end - (start - (17 + base_rtt_size)),
                custom_seq = as.character(getSeq(x = Mmusculus,
                                                 names = chr,
                                                 start = start - (16 + base_rtt_size),
                                                 end = end + (16 + base_rtt_size))))
```
Human example: T allele at chr7:120,971,830-120,971,830
```{r eval= FALSE}

HS <- data.frame('uniq_id' = "eg3.Human", 
                 'chr' = "chr7", 
                 'start' = 120971830, 
                 'end' = 120971830, 
                 'ref' = "T", 
                 'alt' = "G",
                 'pbs_size' = 13, 
                 'rtt_size' = 16, 
                 'pam' = "NGG", # Change to NRN, NYN or NNN if interested in near PAM-less design
                 'genome_build' = "hg19",
                 stringsAsFactors = F) %>%
  dplyr::mutate(base_rtt_size = 16 - (nchar(ref) - nchar(alt)),
                target_pos_start = start - (start - (17 + base_rtt_size)),
                target_pos_end = end - (start - (17 + base_rtt_size)),
                custom_seq = as.character(getSeq(x = Hsapiens, 
                                                 names = chr, 
                                                 start = start - (16 + base_rtt_size), 
                                                 end = end + (16 + base_rtt_size))))
```
&nbsp;  
**Step 2: Run the PE2 design program**

```{r eval= FALSE}
DF_PE2 <- DesignPE2(rbind(ZF, MM, HS))
```
&nbsp;  
**Step 3: Make the R object for the PE3 design program**

Use the sequences 40-90 bp upstream and downstream of the target
```{r eval= FALSE}
ZF_PrePE3 <- ZF %>%
  dplyr::mutate(PE3_seq_pos_start = start - (90 + 16),
                PE3_seq_pos_end = start - (40 - 3 - nchar(pam)),
                PE3_seq_pos = as.character(getSeq(x = Drerio,
                                                  names = chr,
                                                  start = PE3_seq_pos_start,
                                                  end = PE3_seq_pos_end)),
                PE3_seq_neg_start = end + (40 - 3 - nchar(pam)),
                PE3_seq_neg_end = end + (90 + 16),
                PE3_seq_neg = as.character(getSeq(x = Drerio,
                                                  names = chr,
                                                  start = PE3_seq_neg_start,
                                                  end = PE3_seq_neg_end)))

MM_PrePE3 <- MM %>%
  dplyr::mutate(PE3_seq_pos_start = start - (90 + 16),
                PE3_seq_pos_end = start - (40 - 3 - nchar(pam)),
                PE3_seq_pos = as.character(getSeq(x = Mmusculus,
                                                  names = chr,
                                                  start = PE3_seq_pos_start,
                                                  end = PE3_seq_pos_end)),
                PE3_seq_neg_start = end + (40 - 3 - nchar(pam)),
                PE3_seq_neg_end = end + (90 + 16),
                PE3_seq_neg = as.character(getSeq(x = Mmusculus,
                                                  names = chr,
                                                  start = PE3_seq_neg_start,
                                                  end = PE3_seq_neg_end)))

HS_PrePE3 <- HS %>%
  dplyr::mutate(PE3_seq_pos_start = start - (90 + 16),
                PE3_seq_pos_end = start - (40 - 3 - nchar(pam)),
                PE3_seq_pos = as.character(getSeq(x = Hsapiens,
                                                  names = chr,
                                                  start = PE3_seq_pos_start,
                                                  end = PE3_seq_pos_end)),
                PE3_seq_neg_start = end + (40 - 3 - nchar(pam)),
                PE3_seq_neg_end = end + (90 + 16),
                PE3_seq_neg = as.character(getSeq(x = Hsapiens,
                                                  names = chr,
                                                  start = PE3_seq_neg_start,
                                                  end = PE3_seq_neg_end)))
```
&nbsp;  
**Step 4: Run the PE3 design program to get secondary sgRNAs**

```{r eval= FALSE}
DF_PE3 <- DesignPE3(rbind(ZF_PrePE3, MM_PrePE3, HS_PrePE3))
```
&nbsp;  
**Step 5: Make the R object for the PE3b design program**

Swap out the reference sequence with an altered sequence including the target allele
```{r eval= FALSE}
ZF_PrePE3b <- ZF %>%
  dplyr::mutate(uniq_id = paste0(uniq_id, "-b"),
                ref_b = alt,
                alt_b = ref,
                end_b = end + (nchar(ref_b) - nchar(alt_b)), 
                target_pos_end_b = target_pos_end + (nchar(ref_b) - nchar(alt_b)),
                custom_seq_b = paste0(substr(custom_seq, 1, target_pos_start - 1),
                                      ref_b,
                                      substr(custom_seq, target_pos_end + 1, nchar(custom_seq))),
                base_rtt_size_b = rtt_size + (nchar(alt_b) - nchar(ref_b))) %>%
  dplyr::select(uniq_id, chr, start, end_b, ref_b, alt_b, pbs_size, rtt_size, base_rtt_size_b, pam, target_pos_start, target_pos_end_b, custom_seq_b) %>%
  dplyr::rename(ref = ref_b, alt = alt_b, end = end_b,base_rtt_size = base_rtt_size_b, target_pos_end = target_pos_end_b, custom_seq = custom_seq_b) %>%
  dplyr::mutate(PE3b_seq_pos = substr(custom_seq, (target_pos_start - (20 - 1)), (target_pos_start + nchar(pam) - 1 + 3)),
                PE3b_seq_neg = substr(custom_seq, (target_pos_end - (nchar(pam) - 1) - 3), (target_pos_end + 20 - 1)))

MM_PrePE3b <- MM %>%
  dplyr::mutate(uniq_id = paste0(uniq_id, "-b"),
                ref_b = alt,
                alt_b = ref,
                end_b = end + (nchar(ref_b) - nchar(alt_b)), 
                target_pos_end_b = target_pos_end + (nchar(ref_b) - nchar(alt_b)),
                custom_seq_b = paste0(substr(custom_seq, 1, target_pos_start - 1),
                                      ref_b,
                                      substr(custom_seq, target_pos_end + 1, nchar(custom_seq))),
                base_rtt_size_b = rtt_size + (nchar(alt_b) - nchar(ref_b))) %>%
  dplyr::select(uniq_id, chr, start, end_b, ref_b, alt_b, pbs_size, rtt_size, base_rtt_size_b, pam, target_pos_start, target_pos_end_b, custom_seq_b) %>%
  dplyr::rename(ref = ref_b, alt = alt_b, end = end_b,base_rtt_size = base_rtt_size_b, target_pos_end = target_pos_end_b, custom_seq = custom_seq_b) %>%
  dplyr::mutate(PE3b_seq_pos = substr(custom_seq, (target_pos_start - (20 - 1)), (target_pos_start + nchar(pam) - 1 + 3)),
                PE3b_seq_neg = substr(custom_seq, (target_pos_end - (nchar(pam) - 1) - 3), (target_pos_end + 20 - 1)))

HS_PrePE3b <- HS %>%
  dplyr::mutate(uniq_id = paste0(uniq_id, "-b"),
                ref_b = alt,
                alt_b = ref,
                end_b = end + (nchar(ref_b) - nchar(alt_b)), 
                target_pos_end_b = target_pos_end + (nchar(ref_b) - nchar(alt_b)),
                custom_seq_b = paste0(substr(custom_seq, 1, target_pos_start - 1),
                                      ref_b,
                                      substr(custom_seq, target_pos_end + 1, nchar(custom_seq))),
                base_rtt_size_b = rtt_size + (nchar(alt_b) - nchar(ref_b))) %>%
  dplyr::select(uniq_id, chr, start, end_b, ref_b, alt_b, pbs_size, rtt_size, base_rtt_size_b, pam, target_pos_start, target_pos_end_b, custom_seq_b) %>%
  dplyr::rename(ref = ref_b, alt = alt_b, end = end_b,base_rtt_size = base_rtt_size_b, target_pos_end = target_pos_end_b, custom_seq = custom_seq_b) %>%
  dplyr::mutate(PE3b_seq_pos = substr(custom_seq, (target_pos_start - (20 - 1)), (target_pos_start + nchar(pam) - 1 + 3)),
                PE3b_seq_neg = substr(custom_seq, (target_pos_end - (nchar(pam) - 1) - 3), (target_pos_end + 20 - 1)))
```
&nbsp;  
**Step 6: Run the PE3b design program to get secondary sgRNAs**

```{r eval= FALSE}
DF_PE3b <- DesignPE3b(rbind(ZF_PrePE3b, MM_PrePE3b, HS_PrePE3b))
```